package com.techelix.prizebond

import android.app.Activity
import android.content.Context
import com.techelix.prizebond.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import java.lang.ref.WeakReference

class AppClass : DaggerApplication() {


    override fun onCreate() {
        super.onCreate()
        ctx = this
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.factory().create(this)
    }


    companion object {
        lateinit var ctx: Context

        //getting current activity reference to show dialog from non-activity class
        var mContext: WeakReference<Activity>? = null

        fun getAppContext(): Context {
            return ctx
        }
    }

}
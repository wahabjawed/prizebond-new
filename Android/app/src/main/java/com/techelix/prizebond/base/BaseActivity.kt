package com.techelix.prizebond.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import com.techelix.prizebond.util.KeyboardUtil
import com.techelix.prizebond.util.ToastFactory
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity() {


    @Inject
    lateinit var toastFactory: ToastFactory

    @Inject
    lateinit var keyboardUtil: KeyboardUtil


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        attachViewModel()
        startObservingNavEvents()
        setObservers()

    }

    fun popStack() {
        keyboardUtil.closeKeyboard(this@BaseActivity)
        supportFragmentManager.popBackStack()
    }

    /*pop all fragments from stack till desired fragment
   * fragment name will be class.name
   * */

    fun popStackTill(fragmentName: String) {
        val fm = supportFragmentManager
        if (fm!!.backStackEntryCount == 1) {
            popStack()
            return
        }
        for (i in fm!!.backStackEntryCount - 1 downTo 1) {
            if (!fm.getBackStackEntryAt(i).name.equals(fragmentName, true)) {
                fm.popBackStack()
            } else {
                break
            }
        }
    }

    abstract protected fun attachViewModel()
    abstract protected fun setObservers()
    abstract protected fun startObservingNavEvents()

    abstract protected fun showLoader(progressText: String = "")
    abstract protected fun hideLoader()


    open protected fun showToast(message: String) {
        toastFactory.create(message)
    }

    open protected fun showException(message: String) {
        showToast(message)
    }

    open protected fun showError(message: String) {
        showToast(message)
    }

    /**
     *This function will be called by base activity if account manager
     * doesnt contain account, This function will call setupViewModel's forceLagou
     * which will clear local database
     */
    protected abstract fun forceLogout()


    /*Observing events which checks navigations of the user*/
    fun observeDataEvents(viewModel: BaseViewModel) {
        viewModel.obDataEvent.observe(this, Observer {
            var event = it.getEventIfNotHandled()
            if (event != null)
                when (event) {
                    is BaseDataEvent.ShowLoader -> showLoader()
                    is BaseDataEvent.Exception -> showException(event.message)
                    is BaseDataEvent.Error -> showError(event.message)
                    is BaseDataEvent.Toast -> showToast(event.message)
                    BaseDataEvent.HideLoader -> hideLoader()
                    BaseDataEvent.ForceLogout -> forceLogout()
                    null -> println("No Events Found on ObserDataEvent")
                }.exhaustive
        })


    }

    fun addFragmentWithAnimation(
        fragment: Fragment,
        frameId: Int,
        addBackstack: Boolean,
        clearBackStack: Boolean,
        enter: Int,
        exit: Int,
        popEnter: Int,
        popExit: Int
    ) {
        supportFragmentManager.transact {
            setCustomAnimations(enter, exit, popEnter, popExit)
            if (addBackstack)
                addToBackStack(fragment::class.java.name)
            if (clearBackStack) {
                for (i in 0 until supportFragmentManager.getBackStackEntryCount()) {
                    supportFragmentManager.popBackStack()
                }
            }
            add(frameId, fragment)
        }
    }


}

/**
 * Runs a FragmentTransaction, then calls commit().
 */
private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commit()
}
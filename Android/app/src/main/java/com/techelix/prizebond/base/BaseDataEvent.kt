package com.techelix.prizebond.base


sealed class BaseDataEvent {
    class ShowLoader(val progressText: String = "") : BaseDataEvent()
    object HideLoader : BaseDataEvent()
    class Exception(val message: String) : BaseDataEvent()
    class Error(val message: String) : BaseDataEvent()
    class Toast(val message: String) : BaseDataEvent()

    object ForceLogout : BaseDataEvent()
}
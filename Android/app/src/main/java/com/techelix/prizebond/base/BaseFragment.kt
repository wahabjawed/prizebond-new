package com.techelix.prizebond.base

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.techelix.prizebond.R
import com.techelix.prizebond.util.KeyboardUtil
import com.techelix.prizebond.util.ToastFactory
import dagger.android.support.DaggerFragment
import javax.inject.Inject


abstract class BaseFragment : DaggerFragment() {

    protected var ctx: FragmentActivity? = null

    @Inject
    lateinit var keyboardUtil: KeyboardUtil

    @Inject
    lateinit var toastFactory: ToastFactory

    var activityViewModel: BaseViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.ctx = activity
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initializeComponents()
        startObservingNavigation()
        startObservingFragmentNavigation()
    }

    protected inline fun <reified V : ViewModel> attachViewModel(viewModelFactory: ViewModelProvider.Factory): V =
        ViewModelProviders.of(this, viewModelFactory).get(V::class.java)

    protected inline fun <reified T : BaseViewModel> attachNavViewModel(): T {
        activity?.let {
            activityViewModel = ViewModelProviders.of(it).get(T::class.java)
        }

        return activityViewModel as T
            ?: throw ExceptionInInitializerError("Unable to get viewmodel")
    }

    fun popStack() {
        keyboardUtil.closeKeyboard(ctx!!)
        ctx!!.supportFragmentManager.popBackStack()
    }

    protected fun showToast(message: String) {
        toastFactory.create(message)
    }

    protected fun showException(message: String) {
        showToast(message)
    }

    fun hideKeyBoard() {
        keyboardUtil.closeKeyboard(ctx!!)
    }

    fun activateHideKeyboardUponTouchingScreen(view: View) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (view !is EditText) {
            view.setOnTouchListener { v, event ->
                keyboardUtil.closeKeyboard(ctx!!)
                false
            }
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                activateHideKeyboardUponTouchingScreen(innerView)
            }
        }
    }

    fun Fragment.replaceFragmentWithAnimation(
        fragment: Fragment,
        frameId: Int,
        addBackstack: Boolean,
        clearBackStack: Boolean,
        enter: Int,
        exit: Int,
        popEnter: Int,
        popExit: Int
    ) {
        ctx!!.supportFragmentManager.transact {
            setCustomAnimations(enter, exit, popEnter, popExit)
            if (addBackstack)
                addToBackStack(fragment::class.java.name)
            if (clearBackStack) {
                for (i in 0 until ctx!!.supportFragmentManager.getBackStackEntryCount()) {
                    ctx!!.supportFragmentManager.popBackStack()
                }
            }
            replace(frameId, fragment)
        }
    }

    fun Fragment.addFragmentWithAnimation(
        fragment: Fragment,
        frameId: Int,
        addBackstack: Boolean,
        clearBackStack: Boolean,
        enter: Int,
        exit: Int,
        popEnter: Int,
        popExit: Int
    ) {
        ctx!!.supportFragmentManager.transact {
            setCustomAnimations(enter, exit, popEnter, popExit)
            if (addBackstack)
                addToBackStack(fragment::class.java.name)
            if (clearBackStack) {
                for (i in 0 until ctx!!.supportFragmentManager.getBackStackEntryCount()) {
                    ctx!!.supportFragmentManager.popBackStack()
                }
            }
            add(frameId, fragment)
        }
    }

    fun Fragment.addFragmentWithSlideAnimation(
        fragment: Fragment,
        frameId: Int,
        addBackstack: Boolean,
        clearBackStack: Boolean
    ) {
        ctx!!.supportFragmentManager.transact {
            setCustomAnimations(
                R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right
            )
            if (addBackstack)
                addToBackStack(fragment::class.java.name)
            if (clearBackStack) {
                for (i in 0 until ctx!!.supportFragmentManager.getBackStackEntryCount()) {
                    ctx!!.supportFragmentManager.popBackStack()
                }
            }
            add(frameId, fragment)
        }
    }

    fun replaceFragmentWithSlideAnimation(
        fragment: Fragment,
        frameId: Int,
        addBacktack: Boolean,
        clearBackStack: Boolean,
        allowMultipleInstances: Boolean = false
    ) {

        //        double tapping to open fragment twice wont work
        if (!allowMultipleInstances) {
//            checking if fragment is present in back stack
            if (ctx!!.supportFragmentManager.backStackEntryCount > 0 &&
                ctx!!.supportFragmentManager.getBackStackEntryAt(
                    ctx!!.supportFragmentManager.backStackEntryCount - 1
                ).name?.equals(
                    fragment::class.java.name
                )!!
            )
                return
/*checkin if fragment is not in backstack but present is fragments list meaning fragment is added but not placed in backstack*/
            if (ctx!!.supportFragmentManager.fragments.size > 0 &&
                ctx!!.supportFragmentManager.fragments.get(ctx!!.supportFragmentManager.fragments.size - 1)::class.java.name.equals(
                    fragment::class.java.name
                )
            )
                return
        }
        ctx!!.supportFragmentManager!!.transact {
            if (!clearBackStack)
                setCustomAnimations(
                    R.anim.slide_in_right,
                    R.anim.slide_out_left,
                    R.anim.slide_in_left,
                    R.anim.slide_out_right
                )
            else
                setCustomAnimations(
                    R.anim.slide_in_left,
                    R.anim.slide_out_right,
                    R.anim.slide_in_left,
                    R.anim.slide_out_right
                )
            if (addBacktack)
                addToBackStack(fragment::class.java.name)
            if (clearBackStack) {
                for (i in 0 until ctx!!.supportFragmentManager.getBackStackEntryCount()) {
                    ctx!!.supportFragmentManager.popBackStack()
                }
            }
            replace(frameId, fragment)
        }
    }

    fun Fragment.replaceFragment(
        fragment: Fragment, frameId: Int, addBacktack: Boolean,
        clearBackStack: Boolean
    ) {
        ctx!!.supportFragmentManager.transact {
            if (addBacktack)
                addToBackStack(fragment::class.java.name)
            if (clearBackStack) {
                for (i in 0 until ctx!!.supportFragmentManager.getBackStackEntryCount()) {
                    ctx!!.supportFragmentManager.popBackStack()
                }
            }
            replace(frameId, fragment)
        }
    }

    /**
     * Runs a FragmentTransaction, then calls commit().
     */
    private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
        beginTransaction().apply {
            action()
        }.commit()
    }

    override fun onStop() {
        super.onStop()
        keyboardUtil.closeKeyboard(activity!!)
    }

    /*Observing Event which is handled by activity*/
    fun observeDataEvents(viewModel: BaseFragmentViewModel) {
        viewModel.obDataEvent.observe(viewLifecycleOwner, Observer {
            var event = it.getEventIfNotHandled()
            if (event != null)
                when (event) {
                    is BaseDataEvent.ShowLoader -> showLoader(event.progressText)
                    BaseDataEvent.HideLoader -> hideLoader()
                    is BaseDataEvent.Exception -> showException(event.message)
                    is BaseDataEvent.Error -> showError(event.message)
                    is BaseDataEvent.Toast -> showToast(event.message)
                    BaseDataEvent.ForceLogout -> activityViewModel?.forceLogout()
                }.exhaustive
        })
    }

    /*pop all fragments from stack till desired fragment
    * fragment name will be class.name
    * */

    fun popStackTill(fragmentName: String) {
        val fm = fragmentManager
        if (fm!!.backStackEntryCount == 1) {
            popStack()
            return
        }
        for (i in fm!!.backStackEntryCount - 1 downTo 1) {
            if (!fm.getBackStackEntryAt(i).name.equals(fragmentName, true)) {
                fm.popBackStack()
            } else {
                break
            }
        }
    }


    protected abstract fun initializeComponents()
    protected abstract fun startObservingNavigation()
    protected abstract fun startObservingFragmentNavigation()
    protected open fun initializeViews() {}
    abstract protected fun showLoader(progressText: String = "")
    abstract protected fun hideLoader()

    //
    protected fun showError(message: String) {
        showToast(message)
    }

    //
    fun getLastOpenFragment() = fragmentManager?.backStackEntryCount?.minus(1)?.let {
        if (it != -1)
            activity?.supportFragmentManager?.getBackStackEntryAt(
                it
            )?.name
        else
            null
    }


}
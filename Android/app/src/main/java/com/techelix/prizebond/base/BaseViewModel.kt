package com.techelix.prizebond.base


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.NonCancellable.cancel
import java.io.IOException


open class BaseViewModel : ViewModel() {


    var _subscribed: MutableLiveData<BaseEvent<Any>> = MutableLiveData()
    var subscribed: LiveData<BaseEvent<Any>> = _subscribed

    protected val _dataEvent: MutableLiveData<BaseEvent<BaseDataEvent>> = MutableLiveData()
    val obDataEvent: LiveData<BaseEvent<BaseDataEvent>> = _dataEvent

    protected var _error: MutableLiveData<String> = MutableLiveData()
    var error: LiveData<String> = _error

    protected var _exception: MutableLiveData<String> = MutableLiveData()
    var exception: LiveData<String> = _exception

    protected var _success: MutableLiveData<Any> = MutableLiveData()
    var success: LiveData<Any> = _success


    protected fun showLoader(progressText: String = "") {
        _dataEvent.value = BaseEvent(BaseDataEvent.ShowLoader(progressText))
    }

    protected fun hideLoader() {
        _dataEvent.value = BaseEvent(BaseDataEvent.HideLoader)
    }


    protected fun showToast(message: String) {
        _dataEvent.value = BaseEvent(BaseDataEvent.Toast(message))
    }

    //    this method is dummy just to get forcelogout in baseViewModel,
//    actual method exist in BaseActivityViewModel which will be called by Base Activity whenever
//    logout is requried for example unauthenticated error
    open fun forceLogout() {
        _dataEvent.value = BaseEvent(BaseDataEvent.ForceLogout)
    }

    protected fun showError(message: Any?) {
        _dataEvent.value = (BaseEvent(BaseDataEvent.Error(message!! as String)))

    }

    protected fun showException(exception: Exception) {
        if (exception is IOException) {
            showToast("Please check your network connection.")
        } else if (exception is ConnectivityException) {
            showToast("No Internet Connectivity.")
        } else {
//            show message only if it is not connectivity exception
            _dataEvent.value = BaseEvent(BaseDataEvent.Exception(exception.message!!))
        }
        hideLoader()
    }

    /*will cancel all request if viewmodel is dispatched*/

    @InternalCoroutinesApi
    override fun onCleared() {
        super.onCleared()
        cancel()
    }
}
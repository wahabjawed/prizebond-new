package com.techelix.prizebond.base

class ConnectivityException() : Exception("No Internet")

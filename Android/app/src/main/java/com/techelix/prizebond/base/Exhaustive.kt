package com.techelix.prizebond.base

val <T> T.exhaustive: T
    get() = this
package com.techelix.prizebond.di

import android.content.Context
import com.techelix.prizebond.AppClass
import com.techelix.prizebond.di.base_scope.ApplicationScope
import com.techelix.prizebond.di.injector.ActivityInjectBuilder
import com.techelix.prizebond.di.module.StorageModule
import com.techelix.prizebond.di.module.SystemServicesModule
import com.techelix.prizebond.di.module.UtilModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule


@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        ActivityInjectBuilder::class,
        SystemServicesModule::class,
        StorageModule::class,
        UtilModule::class
    ]
)
@ApplicationScope
interface AppComponent : AndroidInjector<AppClass> {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }
}
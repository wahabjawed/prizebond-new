package com.techelix.prizebond.di.base_scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ScreenScope

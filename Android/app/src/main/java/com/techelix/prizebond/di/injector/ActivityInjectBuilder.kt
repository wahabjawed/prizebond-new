package com.techelix.prizebond.di.injector

import com.techelix.prizebond.view.AuthActivity
import com.techelix.prizebond.base.BaseActivity
import com.techelix.prizebond.di.scope.AuthScope
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityInjectBuilder {

    @ContributesAndroidInjector
    abstract fun bindBaseActivity(): BaseActivity

    @ContributesAndroidInjector(modules = [AuthFragmentBuilder::class])
    @AuthScope
    abstract fun bindAuthActivity(): AuthActivity


}
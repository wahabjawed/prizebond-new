package com.techelix.prizebond.di.module

import android.content.Context
import android.content.SharedPreferences
import com.techelix.prizebond.AppClass
import com.techelix.prizebond.di.base_scope.ApplicationScope
import dagger.Module
import dagger.Provides


@Module
object StorageModule {

    @ApplicationScope
    @Provides
    fun provideSharedPreferences(): SharedPreferences =
        AppClass.getAppContext().getSharedPreferences("ASGDJHASGD", Context.MODE_PRIVATE)

}
package com.techelix.prizebond.di.module

import android.content.Context
import android.net.ConnectivityManager
import android.view.inputmethod.InputMethodManager
import com.techelix.prizebond.di.base_scope.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
object SystemServicesModule {

    @JvmStatic
    @Provides
    @ApplicationScope
    fun provideInputMedthodManager(context: Context) =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    @JvmStatic
    @Provides
    @ApplicationScope
    fun provideConnectivityManager(context: Context) =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager


}
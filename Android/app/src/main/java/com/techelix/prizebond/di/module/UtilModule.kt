package com.techelix.prizebond.di.module

import android.content.Context
import com.bumptech.glide.Glide
import com.techelix.prizebond.di.base_scope.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
object UtilModule {

    @JvmStatic
    @Provides
    @ApplicationScope
    fun provideGlide(context: Context) = Glide.with(context)

}
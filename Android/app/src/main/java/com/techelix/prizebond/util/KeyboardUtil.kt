package com.techelix.prizebond.util

import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentActivity
import com.techelix.prizebond.di.base_scope.ApplicationScope
import java.lang.ref.WeakReference
import javax.inject.Inject


@ApplicationScope
class KeyboardUtil @Inject constructor(var inputMethodManager: InputMethodManager) {

    fun closeKeyboard(activity: FragmentActivity) {
        try {
            val context = WeakReference<FragmentActivity>(activity)
            if (context.get() != null) {
                //receiving current window focus
                var view: View? = context.get()!!.currentFocus
                if (view == null)
                //create view object
                    view = View(context.get()!!)
                //hide keyboard
                inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showKeyboard() = try {
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}